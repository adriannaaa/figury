/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package figury;

import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.Ellipse2D;

/**
 *
 * @author adrianna
 */
public class Elipsa extends Figura{
    
     public Elipsa(Graphics2D buf, int del, int w, int h) {
        super(buf, del, w, h);      
        shape = new Ellipse2D.Float(0,0,15,10);
        aft = new AffineTransform();
        area = new Area(shape);
    }
    
}
