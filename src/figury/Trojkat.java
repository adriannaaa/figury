/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package figury;

import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.Path2D;

/**
 *
 * @author adrianna
 */
public class Trojkat extends Figura {
    
    public Trojkat(Graphics2D buf, int del, int w, int h){
        super(buf, del, w, h);
        Path2D path = new Path2D.Double();
        path.moveTo(0, 0);
        path.lineTo(5, 5);
        path.lineTo(10, 0);
        path.closePath();
        
        shape = path;
        aft = new AffineTransform();
        area= new Area(shape);
    }
    
}
